package com.wlhlearn;

import com.wlhlearn.Entity.RpcRequest;

import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;

/**
 * @Author: wlh
 * @Date: 2019/6/11 16:22
 * @Version 1.0
 * @despricate: 网络传输层 处理
 */
public class RpcnetTransport {

    private String host ;

    private int port ;

    public String getHost() {
        return host;
    }

    public void setHost(String host) {
        this.host = host;
    }

    public int getPort() {
        return port;
    }

    public void setPort(int port) {
        this.port = port;
    }


    public RpcnetTransport(String host, int port) {
        this.host = host;
        this.port = port;
    }

    public Object send(RpcRequest rpcRequest){
        Socket socket=null ;
        Object result=null ;
        ObjectInputStream objectInputStream=null ;
        ObjectOutputStream objectOutputStream=null ;

        try {
            socket=new Socket(host,port);
            objectOutputStream=new ObjectOutputStream(socket.getOutputStream());
            objectOutputStream.writeObject(rpcRequest);
            objectOutputStream.flush();

            objectInputStream=new ObjectInputStream(socket.getInputStream());
            result=objectInputStream.readObject();
            return  result;

        }catch (Exception e){
            e.printStackTrace();
        }

         return  null ;
    }
}
