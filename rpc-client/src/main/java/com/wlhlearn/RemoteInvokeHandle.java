package com.wlhlearn;

import com.wlhlearn.Entity.RpcRequest;
import org.omg.CORBA.SystemException;
import org.omg.CORBA.portable.InputStream;
import org.omg.CORBA.portable.InvokeHandler;
import org.omg.CORBA.portable.OutputStream;
import org.omg.CORBA.portable.ResponseHandler;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;

/**
 * @Author: wlh
 * @Date: 2019/6/11 16:11
 * @Version 1.0
 * @despricate:learn
 */
public class RemoteInvokeHandle implements InvocationHandler {

    private String host ;

    private int port ;

    public RemoteInvokeHandle(String host, int port) {
        this.host = host;
        this.port = port;
    }

    @Override
    public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
        System.out.println("invoke");
        RpcRequest rpcRequest=new RpcRequest();
        rpcRequest.setClassName(method.getDeclaringClass().getName());
        rpcRequest.setMethodName(method.getName());
        rpcRequest.setParams(args);

        RpcnetTransport transport=new RpcnetTransport(host,port);
        Object result= transport.send(rpcRequest);
        return result;
    }
}
