package com.wlhlearn;

import com.wlhlearn.Service.IHelloService;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args ) throws Exception {
        RpcProxyClient rpcProxyClient=new RpcProxyClient();


        IHelloService helloService=rpcProxyClient.clientProxy(IHelloService.class,"localhost",8080);

        String result=helloService.sayHello("tom");
        System.out.println(result);
    }
}
