package com.wlhlearn;

import java.lang.reflect.Proxy;

/**
 * @Author: wlh
 * @Date: 2019/6/11 16:08
 * @Version 1.0
 * @despricate:learn
 */
public class RpcProxyClient {


    public <T> T clientProxy(final Class<T> interfacesClass, final String host, final int port) throws Exception {

     return (T)Proxy.newProxyInstance(interfacesClass.getClassLoader(), new Class<?>[]{interfacesClass},
                new RemoteInvokeHandle("localhost", 8080));

    }
}
