package com.wlhlearn;

import com.wlhlearn.Service.IHelloService;
import com.wlhlearn.serverImpl.HelloServiceImpl;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args )
    {
       IHelloService helloService=new HelloServiceImpl() ;
        RpcProxyServer rpcProxyServer=new RpcProxyServer();
        rpcProxyServer.publisher(helloService,8080);

    }
}
