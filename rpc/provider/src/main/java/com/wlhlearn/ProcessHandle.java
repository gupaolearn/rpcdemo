package com.wlhlearn;

import com.wlhlearn.Entity.RpcRequest;

import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.lang.reflect.Method;
import java.net.Socket;

/**
 * @Author: wlh
 * @Date: 2019/6/11 15:35
 * @Version 1.0
 * @despricate:learn
 */
public class ProcessHandle implements Runnable {
    private Socket socket;

    private Object service;

    public ProcessHandle(Socket socket, Object service) {
        this.socket = socket;
        this.service = service;
    }

    @Override
    public void run() {
        try {
            // 处理对应的流信息
            ObjectInputStream inputStream = null;
            ObjectOutputStream objectOutputStream = null;
            inputStream = new ObjectInputStream(socket.getInputStream());
            // 获取请求的方法 参数 等信息
            RpcRequest rpcRequest = (RpcRequest) inputStream.readObject();
            Object result=invoke(rpcRequest);

            objectOutputStream=new ObjectOutputStream(socket.getOutputStream());
            objectOutputStream.writeObject(result);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    public Object invoke(RpcRequest rpcRequest) {
        try {
            Object[] params = rpcRequest.getParams();
            Class<?>[] types = new Class[params.length];

            for (int i = 0; i < params.length; i++) {
                types[i] = params[i].getClass();
            }


            Class clazz = Class.forName(rpcRequest.getClassName());

            Method method = clazz.getMethod(rpcRequest.getMethodName(), types);

             Object result=method.invoke(service, params);
             return  result ;

        } catch (Exception e) {
            e.printStackTrace();
        }
       return  null ;

    }
}
