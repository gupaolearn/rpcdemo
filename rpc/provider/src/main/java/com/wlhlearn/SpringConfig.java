package com.wlhlearn;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Component;

/**
 * @Author: wlh
 * @Date: 2019/6/11 16:55
 * @Version 1.0
 * @despricate:learn
 */
@Configuration
@ComponentScan(basePackages = "com.wlhlearn")
public class SpringConfig {

    @Bean(name="wlhServer")
    public WlhRpcServer wlhRpcServer(){
        return  new WlhRpcServer() ;
    }
}
