package com.wlhlearn;

import org.springframework.stereotype.Component;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * @Author: wlh
 * @Date: 2019/6/11 16:45
 * @Version 1.0
 * @despricate:通过注解发布服务
 *
 */


@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
@Component
public @interface RpcServer {

    Class<?> value();
}
