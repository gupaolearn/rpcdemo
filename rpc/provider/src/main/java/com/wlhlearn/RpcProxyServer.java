package com.wlhlearn;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * @Author: wlh
 * @Date: 2019/6/11 15:32
 * @Version 1.0
 * @despricate:暴露服务
 */
public class RpcProxyServer {

    ExecutorService executorService= Executors.newCachedThreadPool();  // 可以缓存的线程池

    public  void  publisher(Object service,int port)   {
        ServerSocket serverSocket=null ;
        try {
            serverSocket=new ServerSocket(port);
            while (true){
                Socket socket=serverSocket.accept() ;
                // 通过线程的方式来改善效率
               executorService.execute(new ProcessHandle(socket,service));

            }



        } catch (IOException e) {
            e.printStackTrace();
        }finally {
            try {
                serverSocket.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

    }

}
