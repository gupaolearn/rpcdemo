package com.wlhlearn.serverImpl;

import com.wlhlearn.Entity.User;
import com.wlhlearn.RpcServer;
import com.wlhlearn.Service.IHelloService;

/**
 * @Author: wlh
 * @Date: 2019/6/11 15:28
 * @Version 1.0
 * @despricate:learn
 */
@RpcServer(IHelloService.class)
public class HelloServiceImpl implements IHelloService {
    @Override
    public String sayHello(String content) {
        System.out.println("hello "+ content);
        return "hello"+ content;
    }

    @Override
    public String saveUser(User user) {
        System.out.println("save user");
        return "success";
    }
}
