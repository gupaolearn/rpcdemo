package com.wlhlearn;

import org.springframework.beans.BeansException;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Component;

/**
 * @Author: wlh
 * @Date: 2019/6/11 16:52
 * @Version 1.0
 * @despricate:learn
 */
@Component
public class WlhRpcServer  implements ApplicationContextAware,InitializingBean{
    @Override
    public void afterPropertiesSet() throws Exception {



    }

    @Override
    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {

    }
}
