package com.wlhlearn.Entity;

import java.io.Serializable;

/**
 * @Author: wlh
 * @Date: 2019/6/11 15:40
 * @Version 1.0
 * @despricate:  请求参数的实体
 */
public class RpcRequest implements Serializable{


    private String className ;

    private String methodName ;

    private Object[] params ;

    public String getClassName() {
        return className;
    }

    public void setClassName(String className) {
        this.className = className;
    }

    public String getMethodName() {
        return methodName;
    }

    public void setMethodName(String methodName) {
        this.methodName = methodName;
    }

    public Object[] getParams() {
        return params;
    }

    public void setParams(Object[] params) {
        this.params = params;
    }
}
