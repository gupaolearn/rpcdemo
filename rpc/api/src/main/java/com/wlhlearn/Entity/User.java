package com.wlhlearn.Entity;

/**
 * @Author: wlh
 * @Date: 2019/6/11 15:23
 * @Version 1.0
 * @despricate:learn
 */
public class User {

    private  String name ;

    private  Integer age ;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    @Override
    public String toString() {
        return "User{" +
                "name='" + name + '\'' +
                ", age=" + age +
                '}';
    }
}
